import requests
import json
import pandas as pd
import plotly.graph_objs as go
from scipy.integrate import cumtrapz
import numpy as np
from haversine import haversine
import copy
import math
api_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OTUsIm5h' \
            'bWUiOiJlZXNobWFuIiwicm9sZV9pZCI6MSwiaWF0IjoxNTcxMTc0O' \
            'DY0LCJleHAiOjQ3MjQ3NzQ4NjR9.zStUL4mVRvyeMUSXpbxpgZoNx' \
            '2lSHzIcpbpl1_MbJHA'
mapbox_token = 'pk.eyJ1Ijoicnlhbi13b3N0YXJlayIsImEiOiJja2J0aDk5azc' \
               'wOXg2MnFsNnIxcnd3dHRvIn0.WRsSv2EJA-TAqSIdsb72bQ'


def rdp_2d(x, y, tol, idx=None):
    if not idx: idx = [0, len(x) - 1]
    new_idx = []

    for i in range(len(idx) - 1):
        er = np.abs(
            y[idx[i]: idx[i + 1]] -
            ((y[idx[i + 1]] - y[idx[i]]) / (x[idx[i + 1]] - x[idx[i]]) * (x[idx[i]: idx[i + 1]] - x[idx[i]]) +
             y[idx[i]]))
        if np.max(er) > tol:
            new_idx.append(np.argmax(er) + idx[i])

    if len(new_idx) > 0:
        idx += new_idx
        idx.sort()
        return rdp_2d(x, y, tol, idx)
    else:
        return np.array([x[idx], y[idx]])


class OtrDataSet:

    def __init__(self, csv_path):
        self.df = pd.read_csv(csv_path)
        self.df['distance_m'] = cumtrapz(self.df['truck_vehicle_speed'], self.df['logger_time'], initial=0)
        self.calc_smooth_terrain()
        self.df_rdp = self.calc_rdp_hills(2)

    def find_nearest_point_idx(self, lat_lon_pair):
        error = []
        for i in range(len(self.df)):
            error.append(haversine(
                (self.df['gps_latitude'].iloc[i], self.df['gps_longitude'].iloc[i]),
                tuple(lat_lon_pair)
            ))
        return np.argmin(error)

    def create_subset_data_frame(self, start_lat_lon, end_lat_lon):
        start_idx = self.find_nearest_point_idx(start_lat_lon)
        end_idx = self.find_nearest_point_idx(end_lat_lon)
        new_df = copy.deepcopy(self.df.iloc[start_idx:end_idx])
        new_df['distance_m'] = cumtrapz(new_df['truck_vehicle_speed'], new_df['logger_time'], initial=0)
        return new_df

    def calc_smooth_terrain(self):
        smooth_grade = np.zeros(len(self.df))
        smooth_altitude = np.ones(len(self.df)) * self.df['gps_altitude'].iloc[0]
        for i in range(len(self.df)):

            dist_start = max(0, self.df['distance_m'].iloc[i] - 200)
            dist_end = min(self.df['distance_m'].iloc[-1], self.df['distance_m'].iloc[i] + 200)

            idx_start = np.abs(self.df['distance_m'].values - dist_start).argmin()
            idx_end = np.abs(self.df['distance_m'].values - dist_end).argmin()
            smooth_grade[i] = (self.df['gps_altitude'].iloc[idx_end] - self.df['gps_altitude'].iloc[idx_start]) / \
                              (self.df['distance_m'].iloc[idx_end] - self.df['distance_m'].iloc[idx_start])

            if i > 0: smooth_altitude[i] = smooth_altitude[i - 1] + smooth_grade[i - 1] * \
                                           (self.df['distance_m'].iloc[i] - self.df['distance_m'].iloc[i - 1])

        self.df['smooth_grade_perc'] = smooth_grade * 100
        self.df['smooth_elevation_m'] = smooth_altitude

    def calc_rdp_hills(self, tolerance_m, idx=None):
        distance_m = self.df['distance_m'].values
        elevation_m = self.df['smooth_elevation_m'].values
        latitude_deg = self.df['gps_latitude'].values
        longitude_deg = self.df['gps_longitude'].values

        if not idx: idx = [0, len(distance_m) - 1]
        new_idx = []

        for i in range(len(idx) - 1):
            error_m = np.abs(
                elevation_m[idx[i]: idx[i + 1]] -
                ((elevation_m[idx[i + 1]] - elevation_m[idx[i]]) / (distance_m[idx[i + 1]] - distance_m[idx[i]]) *
                 (distance_m[idx[i]: idx[i + 1]] - distance_m[idx[i]]) + elevation_m[idx[i]]
                 ))
            if np.max(error_m) > tolerance_m:
                new_idx.append(np.argmax(error_m) + idx[i])

        if len(new_idx) > 0:
            idx += new_idx
            idx.sort()
            return self.calc_rdp_hills(tolerance_m, idx)
        else:
            return pd.DataFrame(data=dict(
                distance_m=distance_m[idx],
                elevation_m=elevation_m[idx],
                latitude_deg=latitude_deg[idx],
                longitude_deg=longitude_deg[idx],
                idx=idx
            ))

    def to_req_data(self):
        path_dict = dict(
            lat_deg=list(self.df['gps_latitude'].values),
            lon_deg=list(self.df['gps_longitude'].values),
            grade_perc=list(self.df['smooth_grade_perc'].values),
            elevation_m=list(self.df['smooth_elevation_m'].values),
            distance_m=list(self.df['distance_m'].values)
        )

        hill_dict = dict(
            hill_count=len(self.df_rdp) - 1,
            dist_m=list(self.df_rdp['distance_m'].iloc[:-1].values),
            hill_no=list(np.linspace(1, len(self.df_rdp) - 1, len(self.df_rdp) - 1)),
            start_pos=[{
                'lat': self.df_rdp['latitude_deg'].iloc[i],
                'lng': self.df_rdp['longitude_deg'].iloc[i],
                'elev': self.df_rdp['elevation_m'].iloc[i]
            } for i in range(len(self.df_rdp) - 1)],
            end_pos=[{
                'lat': self.df_rdp['latitude_deg'].iloc[i],
                'lng': self.df_rdp['longitude_deg'].iloc[i],
                'elev': self.df_rdp['elevation_m'].iloc[i]
            } for i in range(1, len(self.df_rdp))],
            heading_deg=[(math.atan2(
                self.df_rdp['latitude_deg'].iloc[i+1] - self.df_rdp['latitude_deg'].iloc[i],
                self.df_rdp['latitude_deg'].iloc[i+1] - self.df_rdp['latitude_deg'].iloc[i]
            ) - math.pi/2) * 180/math.pi for i in range(len(self.df_rdp) - 1)],
            grade_perc=list((self.df_rdp['elevation_m'].iloc[1:].values - self.df_rdp['elevation_m'].iloc[:-1].values) /
                        (self.df_rdp['distance_m'].iloc[1:].values - self.df_rdp['distance_m'].iloc[:-1].values) * 100),
            length_m=list(self.df_rdp['distance_m'].iloc[1:].values - self.df_rdp['distance_m'].iloc[:-1].values),
            start_index=list(self.df_rdp['idx'].iloc[:-1]),
            min_vel_m_s=[None] * (len(self.df_rdp) - 1),
            max_vel_m_s=[None] * (len(self.df_rdp) - 1),
            min_soc_perc=[None] * (len(self.df_rdp) - 1),
            max_soc_perc=[None] * (len(self.df_rdp) - 1),
            start_time_s=[None] * (len(self.df_rdp) - 1),
            end_time_s=[None] * (len(self.df_rdp) - 1),
            soc_estimate_perc=[None] * (len(self.df_rdp) - 1),
            avg_vel_m_s=[None] * (len(self.df_rdp) - 1),
            start_dist_m=list(self.df_rdp['distance_m'].iloc[:-1].values),
            end_dist_m=list(self.df_rdp['distance_m'].iloc[1:].values),
            start_elev_m=list(self.df_rdp['elevation_m'].iloc[:-1].values),
            end_elev_m=list(self.df_rdp['elevation_m'].iloc[1:].values),
            delta_elev_m=list(self.df_rdp['elevation_m'].iloc[1:].values - self.df_rdp['elevation_m'].iloc[:-1].values)
        )

        req_body = dict(
            path_data=path_dict,
            temp_c=25,
            hill_data=hill_dict
        )

        return req_body


def lat_lon_list_2_string(lat_lon_list):
    return ','.join(map(lambda x: str(x), lat_lon_list))


def build_request_params(param_dict):
    strings = []
    keys = list(param_dict.keys())
    values = list(param_dict.values())
    for i in range(len(keys)):
        strings.append(str(keys[i]) + '=' + str(values[i]))
    return '&'.join(strings)


def add_distance_to_path_dict(http_response_dict):
    dist_per_step_m = np.roll(np.array(http_response_dict['pathDistancesFeet']), -1) * 0.3048
    dist_m = cumtrapz(dist_per_step_m, initial=0)
    for idx in range(len(http_response_dict['path'])):
        http_response_dict['path'][idx]['distance'] = dist_m[idx]
    return


def add_grade_to_path_dict(http_response_dict):
    temp_df = pd.DataFrame.from_dict(http_response_dict['path'])
    grade = np.gradient(temp_df['elev'], temp_df['distance']) * 100
    for idx in range(len(http_response_dict['path'])):
        http_response_dict['path'][idx]['grade'] = grade[idx]
    return


def plot_geo_zones(http_response_dict):
    fig = go.Figure()

    geo_zone_dict = http_response_dict['zones']['zones']
    for num, zone in enumerate(geo_zone_dict):
        n = zone['bounds']['north']
        e = zone['bounds']['east']
        w = zone['bounds']['west']
        s = zone['bounds']['south']
        fig.add_trace(go.Scattermapbox(
            lat=[n, n, s, s, n],
            lon=[e, w, w, e, e],
            fill='toself',
            mode='lines',
            name='GZ #%i' % num
        ))

    path_df = pd.DataFrame.from_dict(http_response_dict['path'])

    fig.add_trace(go.Scattermapbox(
        lat=path_df['lat'],
        lon=path_df['lng'],
        customdata=np.stack((
            path_df['elev'],
            path_df['grade'],
            path_df['distance']
        ), 1),
        mode='markers',
        marker=dict(
            size=3,
            color=path_df['elev'],
            colorscale='Viridis',
            showscale=True
        ),
        name='Route Path',
        hovertemplate='Latitude [deg]: %{lat:.3f}<br>'
                      'Longitude [deg]: %{lon:.3f}<br>'
                      'Elevation [m]: %{customdata[0]:.0f}<br>'
                      'Grade [%]: %{customdata[1]:.2f}<br>'
                      'Distance [m]: %{customdata[2]:.1f}<br>'
    ))

    fig.update_layout(
        mapbox={'accesstoken': mapbox_token},
        legend=dict(
            orientation='h',
            yanchor='bottom',
            y=1.02,
            xanchor='right',
            x=1
        )
    )

    return fig


def plot_results(http_response_dict, otr_obj: OtrDataSet = None):
    results = [
        pd.DataFrame.from_dict(http_response_dict['meta_data']['optimize_info']['first_result']),
        pd.DataFrame.from_dict(http_response_dict['meta_data']['optimize_info']['last_result'])
    ]

    fig = go.Figure()

    path_df = pd.DataFrame.from_dict(http_response_dict['path'])
    for column in path_df.columns:
        if column != 'distance':
            fig.add_trace(go.Scatter(
                x=path_df['distance'],
                y=path_df[column],
                name='Path - %s' % column
            ))

    '''
    rdp_data = calc_rdp_elevation(path_df, 45)
    fig.add_trace(go.Scatter(
        x=rdp_data[0],
        y=rdp_data[1],
        name='RDP - Elevation'
    ))
    '''

    simple_path_df = pd.DataFrame.from_dict(http_response_dict['linearized_hill_segments'])
    fig.add_trace(go.Scatter(
        x=simple_path_df['distance_from_start'],
        y=simple_path_df['hill_start_elevation'],
        name='Simple Path - elevation'
    ))

    for i in range(len(results)):
        for column in results[i].columns:
            if column not in ['time_s', 'dist_m']:
                fig.add_trace(go.Scatter(
                    x=results[i]['dist_m'],
                    y=results[i][column],
                    name=('Init - ' if i == 0 else 'Final - ') + column
                ))

    if otr_obj:
        otr_df = otr_obj.create_subset_data_frame(
            (path_df['lat'].iloc[0], path_df['lng'].iloc[0]),
            (path_df['lat'].iloc[-1], path_df['lng'].iloc[-1])
        )

        for key in [
            'bms_system_soc', 'truck_vehicle_speed', 'motor_actual_torque',
            'gps_altitude', 'truck_engine_rpms', 'truck_engine_load',
            'truck_transmission_selected_gear'
        ]:
            fig.add_trace(go.Scatter(
                x=otr_df['distance_m'],
                y=otr_df[key],
                name='OTR - %s' % key
            ))

    return fig


def plot_geo_zone_params(http_response_dict, fig=None):
    if not fig:
        fig = go.Figure()

    results = [
        pd.DataFrame.from_dict(http_response_dict['meta_data']['optimize_info']['first_result']),
        pd.DataFrame.from_dict(http_response_dict['meta_data']['optimize_info']['last_result'])
    ]

    for i in range(len(results)):
        for column in []:
            if column not in ['time_s', 'dist_m']:
                fig.add_trace(go.Scatter(
                    x=results[i]['dist_m'],
                    y=results[i][column],
                    name=('Init - ' if i == 0 else 'Final - ') + column
                ))


def output_ala_hyroute(http_response_dict, r_orig):
    # write CSV elevation file
    path_df = pd.DataFrame.from_dict(http_response_dict['path'])
    path_df.rename(columns={'elev': 'elevation'}, errors='raise', inplace=True)
    path_df[['lat', 'lng', 'grade', 'elevation', 'distance']].to_csv('path.csv', index=False)

    # write linear hill segments
    with open('simple_path.json', 'w') as f:
        json.dump(http_response_dict['linearized_hill_segments'], f, indent=4)

    # mcom call for geozone file
    idx = r_orig.url.find('?')
    new_url = r_orig.url[:idx] + '/mcom' + r_orig.url[idx:]
    print('Sending request for mcom geozone file (reruns simulation)...')
    r_mcom = requests.get(new_url, headers=r_orig.request.headers)
    print('Response received!')
    with open('geo_zones.cfg', 'w') as f:
        f.write(r_mcom.text)


lh_port = 3000
first_lat_lon = [43.085618, -75.126288]
second_lat_lon = [43.083179, -75.118496]
# first_lat_lon = [40.305407, -79.673395]
# second_lat_lon = [40.301781, -79.672355]
headers = {'Authorization': 'Bearer ' + api_token}

req_str = 'http://localhost:3000/look_ahead?' \
          'requestType=1&' \
          'distance=25&' \
          'weight=80&' \
          'point=31.095346,-98.190889&' \
          'point=31.095573,-98.190962&' \
          'targetSpeed=64.6&' \
          'truckSpeed=60.2&' \
          'batterySOC=54.0&' \
          'batteryTemp=36&' \
          'vin=Look_Ahead_Web_App&' \
          'max_iter=60&' \
          'min_speed_perc=75&' \
          'min_opt_soc_perc=55&' \
          'max_opt_soc_perc=85&' \
          'sim_time_step_s=4'
req_str = req_str[:-1] + '0.5'
req_str = None

otr = None
otr_body = None
otr = OtrDataSet('Syracuse_NY_to_Schenectady_NY/OTR_Data/C500_2019-08-28_16-50-07_3739sTo5222s.csv')
# otr = OtrDataSet('Validation Data/102-002_11_02_2020_050813_Hybrid_full-load_281_FuelSavings_DR_joe.csv')
# otr = None
otr_body = otr.to_req_data() if otr else None
if otr:
    first_lat_lon = [otr.df['gps_latitude'].iloc[0], otr.df['gps_longitude'].iloc[0]]
    second_lat_lon = [otr.df['gps_latitude'].iloc[1], otr.df['gps_longitude'].iloc[1]]

params = dict(
    weight=120,
    distance=25,
    point=lat_lon_list_2_string(first_lat_lon) + '&point=' + lat_lon_list_2_string(second_lat_lon),
    requestType=1,  # value of 1 signifies call to power assist
    targetSpeed=65,
    batterySOC=86,
    batteryTemp=30,
    truckSpeed=58,
    vin='888DDD',
    sim_time_step_s=4,
    min_opt_soc_perc=20
)

if otr_body: params['requestType'] = 2  # set request type to 'power assist from otr data' if necessary

fig = go.Figure()
fig.add_trace(go.Scatter(x=otr.df['distance_m'], y=otr.df['gps_altitude'], name='gps_altitude'))
fig.add_trace(go.Scatter(x=otr.df_rdp['distance_m'], y=otr.df_rdp['elevation_m'], name='rdp_elevation_m'))
fig.add_trace(go.Scatter(x=otr.df['distance_m'], y=otr.df['gps_latitude'], name='gps_latitude'))
fig.add_trace(go.Scatter(x=otr.df['distance_m'], y=otr.df['gps_longitude'], name='gps_longitude'))
fig.show()

print('Sending request for simulation...')
'''
r = requests.get(
    ('http://localhost:%i/look_ahead?' % lh_port) + param_string if not req_str else req_str,
    headers=headers
)
'''
param_string = build_request_params(params)
r = requests.request(
    method='get',
    url=('http://localhost:%i/look_ahead?' % lh_port) + param_string if not req_str else req_str,
    data={'body_data': json.dumps(otr_body)},
    headers=headers
)

print('Response received!')

json_dict = json.loads(r.text)
add_distance_to_path_dict(json_dict)
add_grade_to_path_dict(json_dict)

fig_gz = plot_geo_zones(json_dict)
fig_gz.show()

fig_res = plot_results(json_dict, otr_obj=otr)
fig_res.show()

# output_ala_hyroute(json_dict, r)

print()
